# -*- coding: utf-8 -*-
"""
Created on Mon Aug  3 11:40:56 2020

@author: lacos
"""
import numpy as np

"""Paramètres"""

"Nombre de larves"
Nl = 12000
"Nombre d'adulte /2"
Na = 5000

nmut = 150

Tmax= 300
Tintro=301

k=20000

parm=[4,4]

nlarve = 5

#Matrices de survie
df = "F"
fp = "F"

phi0 = 0.01
phi1 = 0.01
phi2 = 0.01
phi01 = 0.01
phi02 = 0.01
phi12 = 0.01

r = 5
#GenoSurv = np.array([[0.95,0.95,0.95],[0.95,0.10,0.95],[0.95,0.95,0.20]])
bic= 0.70
sil= bic*0.4
tar= 0.70
PhenoSurv = [sil,bic,tar]

#densité dependance

dd = "F"

alpha = 0
mu = k/10
delta_mu = 0
sigma = k/8
delta_sigma = 0

#Matrice de dominance
matdom = np.array([[0,1,2],[1,1,1],[2,1,2]])

#Heterogamie

h = "T"

P11 = 0.1
P10 = 0.25

P00 = P11/0.6
P01 = 0.25
P02 = 0.75


P22 = 0.15
P12 = 0.75

cost = "F"
#Reco = "Pheno"
###############################################################################
