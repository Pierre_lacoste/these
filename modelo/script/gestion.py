# -*- coding: utf-8 -*-
"""
Created on Thu Oct  1 11:46:18 2020

@author: lacos
"""

"Gestion des sorties"

"Manipulation des sorties"

import os


dos = os.listdir("./sortie")

num = "38"
fichier = 5
all_file = "T"


for i in dos :
    rep = os.listdir("./sortie/"+i)
    t = open("./sortie/"+i+"/data"+num+".txt","w")
    t.close()
    if all_file == "T":
        for j in  rep[0:(len(rep)-1)]:
            file=open("./sortie/"+i+"/"+j+"/"+os.listdir("./sortie/"+i+"/"+j)[fichier], "r")
            chaine = file.read()
            file.close()
        
            t  = open("./sortie/"+i+"/data"+num+".txt","a")
            t.write("\n"+chaine.split("\n",1)[-1])
            t.close()
    else:
        file=open("./sortie/"+i+"/"+rep[0]+"/"+os.listdir("./sortie/"+i+"/"+rep[0])[fichier], "r")
        chaine = file.read()
        file.close()
        
        t  = open("./sortie/"+i+"/data"+num+".txt","a")
        t.write("\n"+chaine.split("\n",1)[-1])
        t.close()

if fichier == 5:
    data = open("./data"+num+".txt","w")
    data.write("Temps;Fa;Fb;Fc;Faa;Fab;Fac;Fbc;Fbb;Fcc;FA;FB;FC;N;phi1;phi2;Stade;replicat;parm")
    data.close()
elif fichier == 0:
    data = open("./data"+num+".txt","w")
    data.write("homo;01;02;12;total;temps")
    data.close()
elif fichier == 5:
    data = open("./data"+num+".txt","w")
    data.write("homo;01;02;12;total;temps")
    data.close()
elif fichier == 1:
    data = open("./data"+num+".txt","w")
    data.write("X0;X1\n")
    data.close()



for i in dos:
     t  = open("./sortie/"+i+"/data"+num+".txt","r")
     chaine = t.read()
     t.close()
     
     data = open("./data"+num+".txt","a")
     data.write("\n"+chaine)
     data.close()
