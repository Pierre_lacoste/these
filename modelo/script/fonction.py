# -*- coding: utf-8 -*-
"""
Created on Mon Jun 29 14:17:13 2020

@author: lacos
"""

#Packages 
from __future__ import division
from vpython import combin
import random
from obj import individu as indiv
import numpy as np
import math
import matplotlib.pyplot as plt
from scipy.stats import skewnorm

###############################################################################
#Fonction binom => clacule la probalité d'obtenir k suivant une loie binomiale 
#de paramètres n et p 
def binom(k,n,p):
    """binom(k,n,p): probabilité d'avoir k réussite(s) dans n évènements indépendants, chaque évènement ayant une probabilité p% de réussite"""
    x=combin(n,k)*pow(p,k)*pow(1-p,n-k)
    return (x)


###############################################################################
#Fonction hbinom => calcule une (ou plusierus) réalisation d'une variable 
#aléatoire qui suit une loie binomiale de paramètres n et p
def hbinom(n,p,nb=0):
    """hbinom(n,p,nb=0): Génération de valeurs tirées au hasard selon une distribution binomiale"""
    def _hbinom(n,p):
        ph=random.random()
        i=0
        pc=binom(i,n,p)
        while ph>pc:
            i+=1
            pc+=binom(i,n,p)
        return i
    if nb==0:
        return _hbinom(n,p)
    else:
        R=[]
        for j in range(0,nb):
            R.append(_hbinom(n,p))
        return (R)

###############################################################################
    
def weibull (t,k,l):
    S = math.exp(-(t/l)**k)
    return (S)

###############################################################################
#Calcul du fardeau quand on autorise un fardeau non constant (pas utilisé)

def phi (phin,Fxx,Fxy,r):
    if Fxx > 0:
        phi = phin/(1-(Fxx/(Fxy*r)))
    else:
        phi = phin
    return (phi)

###############################################################################
#Fonction surv => determine si un individu meurt, selon sa probabilité de survie p 

def surv (indiv,p):
    """Mort d'un individu selon une probabilité p"""
    
    #Tirage d'une experience de bernoulli avec une probabilité de succè p
    s=hbinom(1,p)
    
    #Si c'est un echec
    if s==0:
        #Alors on suprime l'objet, l'individu est mort
        return []
    else:
        #Sinon on conserve l'objet, l'individu est toujours en vie
        return (indiv)
###############################################################################
#Fonction surv => determine si un individu meurt, selon sa probabilité de survie p 

def surva (indiv,p, phenoeff):
    """Mort d'un individu selon une probabilité p"""
    
    #Tirage d'une experience de bernoulli avec une probabilité de succè p
    s=hbinom(1,p)
    
    #Si c'est un echec
    if s==0:
        #Alors on suprime l'objet, l'individu est mort
        phenoeff[indiv.pheno] += -1
        return [[],phenoeff]
    else:
        #Sinon on conserve l'objet, l'individu est toujours en vie
        return [indiv,phenoeff]


###############################################################################

# Fonction LarveSurv => parcoure la liste des larve pour determiner celles qui 
#survivent    
    
def LarveSurv (listeind, genosurv,N ,k ,predprob = 0.1):
    """Fonction survie larvaire"""
    
    
    liste = []
    
    #Pour chaque larve de la liste 
    for i in listeind :
        #Calcul de la probabilité de survie de la larve en fonction de son 
        #génotype
        p = genosurv[i.x1,i.x2]#-predprob
        #Test de survie
        ind=surv(i,p)
        
        #On ne stocke que les individus qui on survécu
        if ind != []:
            pc = 1-N/k
            
            if pc<0 :
                ind = []
            else:
                ind = surv(ind, pc)
                if ind != []:
                    #Stockage du resultat
                    liste.append(ind)
                else:
                    continue
        else:
            continue
        
    return (liste)
    
###############################################################################
# Fonction LarveSurv => parcoure la liste des adultes pour determiner ceux qui 
#survivent 

def AdultSurv (listeind, genosurv, phenosurv, phenoeff,parm,dd,proba_X0=0, proba_X1=0):
    """Fonction survie adulte"""
    
    
    liste = []
    proba0 = str("")
    proba1 = str("")
    
    for i in listeind :
        #Calcul de la probabilité de survie de l'individu en fonction de son 
        #phénotype
        if dd == "T":
            
            if i.pheno == 0 :
                #print("X=",Ppred)
                Ppred = proba_X0[phenoeff[0]]
                proba0 += str(Ppred) +";\n"
            else :
                #print("X1=",Ppred)
                Ppred = proba_X0[phenoeff[1]]
                proba1 += str(Ppred) +";\n"
                
            
        else :
            Ppred = phenosurv[i.pheno]
        Pintr = weibull(i.age, parm[0], parm[1])
        
        
        #Test de survie
        s=surva(i,Ppred,phenoeff)
        ind = s[0]
        phenoeff = s[1]
        
        if ind != []:
            sv=surva(i,Pintr,phenoeff)
            ind = sv[0]
            phenoeff = sv[1]
         #On ne stocke que les individus qui on survécu
        
            if ind != []:
                ind.viellir ()
                #Stockage du resultat
                liste.append(ind)
            else:
                continue
        else:
            continue
    
    return ([liste,[proba0,proba1]])
    
###############################################################################
# Fonction cycle => realise un cycle de vie, survie larvaire, survie adulte, 
#métamorphose des larves qui on survécu en adulte.

def cycle (ListeLarve, ListeMale, ListeFemelle, genosurv, phenosurv, phenoeff, N, k, parm,dd,proba_X0, proba_X1):
    """ Fonction cylce de vie"""
    
    #Survie des larves
    ListeLarve = LarveSurv(ListeLarve, genosurv, N, k)
    

    
    #Survie des adultes
    Male = AdultSurv(ListeMale, genosurv, phenosurv, phenoeff, parm,dd,proba_X0, proba_X1)
    Femelle = AdultSurv(ListeFemelle, genosurv, phenosurv, phenoeff, parm,dd,proba_X0, proba_X1)
    ListeMale = Male[0]
    probaM = Male[1]
    ListeFemelle = Femelle[0]
    probaF = Femelle[1]
    
    proba0 = probaM[0] + probaF[0]
    proba1 = probaM[1] + probaF[1]
    
    #Métamorphose des larves qui restent
    
    #Pour chaque larve 
    for i in ListeLarve :
        #On tire le sexe de l'individu
        sexe = random.randrange(0,2)
        phenoeff[i.pheno] += 1
        #On attribut le sexe
        if sexe == 0:
            ind = indiv.Male (i.x1,i.x2,i.pheno, 1)
            ListeMale.append(ind)
        elif sexe == 1:
            ind = indiv.Femelle (i.x1,i.x2,i.pheno, 1)
            ListeFemelle.append(ind)
    ListeLarve = []
    return ([ListeLarve, ListeMale, ListeFemelle,proba0,proba1]) 
    
   
###############################################################################
#Fonction accouplement => production des larves à partir de 2 individus
    
def mating (male, femelle, nLarve, matdom):
    """Fonction accouplement"""
    
    #On reunit les allèles des chromosomes x1 et x2 de la femelle et du mâle dans 2 listes.
    chmale = [male.x1,male.x2]
    chfemelle = [femelle.x1,femelle.x2]
    
    #On tire la nombre de larve produit lors de l'accouplement 
    nb_larve = random.randrange(0,nLarve)
    
    femelle.nacc = 1
    femelle.malegeno = chmale
    femelle.offspring = nb_larve
    #Créaion de la liste des lzrves produite par l'accouplement
    listbb = []
    
    i=0
    
    #Pour chaque nouvelles larves
    while i < nb_larve:
        
        #On tire les deux allèles de façon aléatoire 
        x1ind = random.choice(chmale)
        x2ind = random.choice(chfemelle)
        
        #On dertermine de phénotype en fonction du génotype
        X = matdom[x1ind,x2ind]
        
        #On cré le nouvel individu et on le stocke dans la liste des larves 
        ind = indiv.Larve(x1ind,x2ind,X)
        listbb.append(ind)
        
        i += 1
    
    return (listbb)

###############################################################################
#Fonction ponte => production de larve à partir de 1 individu (femelle fécondée)

def ponte (Femelle, nLarve, matdom):
    """Fonction ponte pour une femelle fécondée"""
    
    chmale = Femelle.malegeno
    chfemelle = [Femelle.x1,Femelle.x2]
    
    #On tire la nombre de larve produit lors de l'accouplement 
    nb_larve = random.randrange(0,nLarve)
    
    Femelle.offspring += nb_larve
    #Créaion de la liste des lzrves produite par l'accouplement
    listbb = []
    
    i=0
    
    #Pour chaque nouvelles larves
    while i < nb_larve:
        
        #On tire les deux allèles de façon aléatoire 
        x1ind = random.choice(chmale)
        x2ind = random.choice(chfemelle)
        
        #On dertermine de phénotype en fonction du génotype
        X = matdom[x1ind,x2ind]
        
        #On cré le nouvel individu et on le stocke dans la liste des larves 
        ind = indiv.Larve(x1ind,x2ind,X)
        listbb.append(ind)
        
        i += 1
    
    return (listbb)

###############################################################################
#Construit la matrice du choix de partenaire (en cour)
#2 modalité de choix : par phénotype ou par génotype

def heterogamie (P00,P11,P22,P01,P02,P12):#,Reco):
    """
    if Reco == "Geno":
        AR = np.array(["R","A","A","A","A","A","A","R","A","A","A","R","A","A","A","R"])
    elif Reco == "Pheno":
        AR = np.array(["R","A","A","A","A","R","R","R","A","R","R","R","A","R","R","R"])
    
    
    i=0
    while i < 16:
        if AR[i]=="A":
            h[i]= PA
        
        elif AR[i]=="R":
            h[i]= PR
        
        i += 1
    """
    h = np.array([P00,P01,P02,P01,P11,P12,P02,P12,P22])
    h=h.reshape(3,3)
    
    return h
    
###############################################################################
#Fonction reproduction => realise les accouplement entre mes individus

  
def repro (ListeFemelle , ListeMale, nLarve, h , MatH, matdom, cost):
    """Fonction reproduction"""
    
    Acc = np.array([[0,0,0],[0,0,0],[0,0,0]])
    Rej = np.array([[0,0,0],[0,0,0],[0,0,0]])
    total = 0

    ListeLarve = []
    #Pour chaque femelle
    for i in ListeFemelle : 
        
        #Si la femelle s'est déja accouplé 
        if i.nacc > 0:
            Larve = ponte(i, nLarve, matdom)
        #Sinon 
        else :
            #On tire un mâle aléatoire
            j = random.randrange(0,len(ListeMale))
            male = ListeMale[j]
            
            # Si on intègre un choix de parteniare
            if h == "T":
                #Probabilité d'accouplement tiré dans la matrice des accouplement MatH
               p = MatH[i.pheno,male.pheno]
               #Test si l'accouplement à lieux 
               choice = hbinom(1,p)
               
               #Si c'est un rejet
               if choice == 0:
                   
                   #on enregistre le rejet dans le tableau pour le conserver
                   Rej[male.pheno,i.pheno] += 1
                   total += 1

                   #Si le cout au choix est actif
                   if cost == "T":
                       #On passe à la femelle suivante
                       continue
                   #Si il n'y a pas de cout
                   elif cost == "F" :
                       #Tant que la femelle n'est pas accouplée
                       while choice == 0:
                            #On refait comme précedement
                            j = random.randrange(0,len(ListeMale))
                            male = ListeMale[j]
                            p = MatH[i.pheno,male.pheno]
                            choice = hbinom(1,p)
                            Rej[male.pheno,i.pheno] += 1
                            total += 1
                       else :
                            #Quand la femelle est accouplée elle produit des larves
                            Acc[male.pheno,i.pheno] += 1
                            total += 1
                            Larve = mating(male,i, nLarve, matdom)
               elif choice == 1:
                   #Quand la femelle est accouplée elle produit des larves
                   Acc[male.pheno,i.pheno] += 1
                   total += 1
                   Larve = mating(male,i, nLarve, matdom)
            
            #Sans choix de partenaire
            elif h == "F":
                #On realise l'acouplement
                Acc[male.pheno,i.pheno] += 1
                Larve = mating(male,i, nLarve, matdom)
        
            
            #On stocke les chenilles produites dans la nouvelle liste chenille
        for j in Larve:
            ListeLarve.append(j)
            
    Vrej = [Rej[0,0]+Rej[1,1]+Rej[2,2],Rej[0,1]+Rej[1,0],Rej[0,2]+Rej[2,0],Rej[1,2]+Rej[2,1],total]
    Vacc = [Acc[0,0]+Acc[1,1]+Acc[2,2],Acc[0,1]+Acc[1,0],Acc[0,2]+Acc[2,0],Acc[1,2]+Acc[2,1],total]
    #print("Rejet\n Homo  01 02 12 Total\n  {0}  {1} {2} {3}  {4}".format(Vrej[0],Vrej[1],Vrej[2],Vrej[3],Vrej[4]))
    #print("Accouplement\n Homo  01 02 12 Total\n  {0}  {1} {2} {3}  {4}".format(Vacc[0],Vacc[1],Vacc[2],Vacc[3],Vacc[4]))

    return ([ListeLarve,Vrej,Vacc])



###############################################################################
#Fonction comptage => retourne les infos importantes 


def count (Listeindiv , compteur, stade):
    """Compteur, permet de mesurer les fréquence alléliques , phénotypiques et 
    génotypiques"""
    
    #On définit les matrices de comptage 
    
    #alléles
    alleff=np.array([0,0,0])
    
    #Phénotypes
    phenoeff=np.array([0,0,0])
    
    #Génotypes
    genoeff=np.array([[0,0,0],[0,0,0],[0,0,0]])
    
    pop = Listeindiv
    
    #Taille de la population 
    N=len(pop)
    compteur.n=N
    
    #Pour chaque individu
    for i in pop:
        
        #On incrémente les cases qui correspondent au phénotype, à chaques 
        #allèles et au génotype de l'individu
        phenoeff[i.pheno] +=1
        
        alleff[i.x1] += 1
        alleff[i.x2] += 1
        
        genoeff[i.x1,i.x2] += 1
    
    #On calcule et on stocke les fréquences
    compteur.fa = alleff[0] / (N*2)
    compteur.fb = alleff[1] / (N*2)
    compteur.fc = alleff[2] / (N*2)
    compteur.faa = genoeff[0,0] / N
    compteur.fab = (genoeff[0,1] + genoeff[1,0]) / N
    compteur.fac = (genoeff[0,2] + genoeff[2,0]) / N
    compteur.fbc = (genoeff[1,2] + genoeff[2,1]) / N
    compteur.fbb = genoeff[1,1] / N
    compteur.fcc = genoeff[2,2] / N
    compteur.fA = phenoeff[0] / N
    compteur.fB = phenoeff[1] /N
    compteur.fC = phenoeff[2] /N
    #print(stade,"\n",compteur)
    print("fA = ",compteur.fA)
    print("fB = ",compteur.fB)
    print("fC = ",compteur.fC)
    print("N = ",compteur.n)
    return (compteur)

###############################################################################
#def 

###############################################################################

def chaine (eff,temps,stade, phi1,phi2):
    #On initialise la chaine de char
    line="\n"
    #On ajoute le temps 
    line+=str(temps)
    #On ajoute chaque de l'objet avec comme sep ";"
    line+=";"
    line+=str(eff.fa)
    line+=";"
    line+=str(eff.fb)
    line+=";"
    line+=str(eff.fc)
    line+=";"
    line+=str(eff.faa)
    line+=";"
    line+=str(eff.fab)
    line+=";"
    line+=str(eff.fac)
    line+=";"
    line+=str(eff.fbc)
    line+=";"
    line+=str(eff.fbb)
    line+=";"
    line+=str(eff.fcc)
    line+=";"
    line+=str(eff.fA)
    line+=";"
    line+=str(eff.fB)
    line+=";"
    line+=str(eff.fC)
    line+=";"
    line+=str(eff.n)
    line+=";"
    line+=str(phi1)
    line+=";"
    line+=str(phi2)
    line+=";"
    line+=stade
    line+=";"
    line+=str(eff.replicat)
    line+=";"
    line+=str(eff.parm)
    #Fin pour
    return (line)

###############################################################################


def chaine2 (liste,nom):
    line="\n"
    j=0
    for i in liste:
        line += nom[j]
        line += " = "
        line += str(i)
        line += "\n"
        line += "\n"
        j += 1
    return (line)

###############################################################################

def chaine3 (V,temps):
    line = "\n"
    for i in V:
        line += str(i)
        line += ";"
    line += str(temps)
    return (line)
        


###############################################################################

def skewplot(a,loc,scale):
    
    fig, ax = plt.subplots(1, 1)
    x = np.linspace(skewnorm.ppf(0.0001, a,loc=loc, scale = scale),skewnorm.ppf(0.999999, a,loc=loc, scale = scale), 100)
    ax.plot(x, skewnorm.pdf(x, a,loc=loc, scale = scale),'r-', lw=5, alpha=0.6, label='skewnorm pdf')
    rv = skewnorm(a,loc=loc, scale = scale)
    ax.plot(x, rv.pdf(x), 'k-', lw=2, label='frozen pdf')
    r = skewnorm.rvs(a,loc=loc, scale = scale, size=10000)
    ax.hist(r, density=True, histtype='stepfilled', alpha=0.2)
    ax.legend(loc='best', frameon=False)

    plt.show()


