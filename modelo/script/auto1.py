# -*- coding: utf-8 -*-
"""
Created on Thu Sep 24 16:33:36 2020

@author: lacos
"""

import time
import os

#path = os.path()
t0=time.time()
#Valeurs par défault des paramètres 
k = 2000
Tintro=1000
phi1=0.9
phi2=0.9
fp="'F'"
h="'T'"
P00 = 0.90
P11 = 0.45
P22 = 0.1
deltat_mu = 0
deltat_sigma = 0
path = "C:/Users/adminlocal/Documents/These/data/these.git/modelo/script/"

ref = open(path + "parametre.py","r")
chaine = ref.read()
ref.close()

M_ref = open(path + "main.py","r")
script = M_ref.read()
M_ref.close()

#sigma_test = [2,4,6,8,10,20] # on augmente la protection par la communauté overall
#delta_mu_test = [-5000,-2500,2500,5000] #On fait varier l'avantage - = X0 a un avantage 
#et + = X1 a un avantage
#delta_sigma_test = [0,50,100]
#sil_test = [0.1,0.2,0.4,0.6,0.9]
#tintro_test = [1,100]
Delta_PI = [0.6]
Delta_S = [0.4]

parm_def= [0]
listparm = ["'Delta S","Delta PI"]

#a=0
for i in Delta_S:
    #parm_test = [0.9]#,0.9,0.9]
    for j in Delta_PI:
        parm = parm_def
        if j == "'T'":
            string= listparm[0]+"_T'"
        elif j == "'F'":
            string= listparm[0]+"_F'"
        else:
            string= listparm[0]+"_"+str(i)+"_"+listparm[1]+"_"+str(j)+"'"
        print(string)
        parm = [i,j]
        
        print(string)
        
        parametre = open(path + "parametre_auto.py","w")
        parametre.write(chaine.format(parm[0],parm[1])) 
        parametre.close()
        
        replicat = 0 
        while replicat < 1:
            s =  open(path + "main/main_test"+string+str(replicat)+".py","w")
            s.write(script.format(replicat,string,listparm[0]+"_"+str(i)+"_"+listparm[1]+"_"+str(j)+"'"))
            s.close()
            print(replicat)
            runfile(path+"main/main_test"+string+str(replicat)+".py", wdir = path)
            replicat = replicat + 1
           
    #a+=1
    

t1=time.time()

print(t1-t0)
 