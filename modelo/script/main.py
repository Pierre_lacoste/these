# -*- coding: utf-8 -*-
"""
Created on Wed Jun 24 15:38:26 2020

@author: lacos
"""
#Librairie
import numpy as np
import random as rand
import os
import shutil
import time
from scipy.stats import skewnorm

#Main code

#Module

#Classes
from obj import individu as ind
from obj.count  import *

#Fonctions
from fonction import *

#Paramètres
from parametre_auto import  *
#from parametre import  *

###############################################################################
#Initialiisation

#Date initiale
t0=time.time()

#Création des listes larve, male et femelle
listLarve = []
listMale=[]
listFemelle=[]

#Mutant 
Larvemut=ind.Larve(0,1,1)
larvemut2=ind.Larve(0,2,2)

#Matrice de survie
if fp == "F" :
    GenoSurv = np.array([[(1-phi0),1-phi01,1-phi02],[1-phi01,(1-phi1),1-phi12],[1-phi02,1-phi12,(1-phi2)]])
elif fp == "T":
    GenoSurv = np.array([[(1-phi0),0.95,0.95],[0.95,(1-phi1),(1-phi1)],[0.95,(1-phi1),(1-phi2)]])
#Token
i=1
j=1

if h == "T":
    MatH=heterogamie(P00,P11,P22,P01,P02,P12)
elif h == "F":
    MatH=[]

#Coinstruction des objets et stokage dans les listes 

#Larves
l= ind.Larve (0,0,0) 
while i <= (Nl-nmut):
    listLarve.append(l)
    i += 1

#Ajout des mutants dans la liste Larve 
mut = 1
while mut <= nmut:
    listLarve.append(Larvemut)
    mut += 1
#listLarve.append(larvemut2)

#Adultes
age = []
a=1
while a <= 2*Na:
    age.append(rand.randint(1, 6))
    a+=1
print(age)


token = 0
while j <= Na:
    m = ind.Male(0,0,0,age[token])
    f = ind.Femelle(0,0,0,age[token+1])
    listMale.append(m)
    listFemelle.append(f)
    j += 1
    token

phenoeff = [2*Na,0,0]

#Etat initial de mon sytème 
freq =[count(listLarve, etat(0,0,0,0,0,0,0,0,0,0,0,0,len(listLarve),{0},{1}),stade="Larve"), count(listMale+listFemelle, etat (0,0,0,0,0,0,0,0,0,0,0,0,len(listMale+listFemelle),{0},{1}),stade="Adulte")]
#freq = [count(listLarve, etat(0,0,0,0,0,0,0,0,0,0,0,0,len(listLarve),"test","test"),stade="Larve"), count(listMale+listFemelle, etat (0,0,0,0,0,0,0,0,0,0,0,0,len(listMale+listFemelle),"test","test"),stade="Adulte")]

if dd == "T":
    #Calcul de mu et sigma
    mu = [mu,mu-delta_mu]
    sigma = [sigma,sigma-delta_sigma]

    #Matrice de probabilité de survie 
    proba_X0 = np.zeros(k+1)
    proba_X1 = np.zeros(k+1)

    for i in list(range(0,k+1)) :
        proba_X0[i] = skewnorm.cdf(i,a = alpha,loc = mu[0],scale = sigma[0])
        proba_X1[i] = skewnorm.cdf(i,a = alpha,loc = mu[1],scale = sigma[1])
else:
    proba_X0 = []
    proba_X1 = []


###############################################################################
#Initialisation des fichiers de sortie

#Time stamp
temps = time.localtime(t0)
path = "C:/Users/adminlocal/Documents/These/data/these.git/modelo/script"
path+="/sortie/"+{1}
#path+="./sortie/"+"test/"

if os.path.exists(path):
    print("erreur")
else:
    os.mkdir(path)  

path+="/"
sec = [2,1,0,3,4,5]
for i in sec:
    
    path += str(temps[i])
    path += "_"

if os.path.exists(path):
    path2=path+"(1)"
    num=1
    while os.path.exists(path2):
        num+=1
        path2 = path + "(" +str(num) + ")"
        
    path=path2
    os.mkdir(path)
else :
    os.mkdir(path)

#Fichier Sortie

line= path
line += "/sortie"
sec = [2,1,0,3,4,5]

for i in sec:
    line += "_"
    line += str(temps[i])
    
line += ".txt"
file = open(line,"w")
file.write("Temps;Fa;Fb;Fc;Faa;Fab;Fac;Fbc;Fbb;Fcc;FA;FB;FC;N;phi1;phi2;Stade;replicat;parm")
file.close()

stringL=chaine(freq[0],0,"Larve","NA","NA")
stringA=chaine(freq[1],0,"Adulte",phi1,phi2)
file = open(line,"a")
file.write(stringL)
file.write(stringA)
file.close()

#Fichier Parametre

line2= path
line2 += "/parametre"

for i in sec:
    line2 += "_"
    line2 += str(temps[i])
line2 += ".txt"

file2 = open(line2,"w")
file2.write("Paramètre du modèle\n")
file2.close()

#Fichier accouplement / rejet
line3= path
line3 += "/Accouplement"

for i in sec:
    line3 += "_"
    line3 += str(temps[i])
line3 += ".txt"

file3 = open(line3,"w")
file3.write("homo;01;02;12;total;temps")
file3.close()

line4= path
line4 += "/Rejet"

for i in sec:
    line4 += "_"
    line4 += str(temps[i])
line4 += ".txt"

file4 = open(line4,"w")
file4.write("homo;01;02;12;total;temps")
file4.close()

nom=list(["Nl","Na","Tmax","Tintro","k","parm","nlarve","GenoSurv","PhenoSurv","h","P00","P11","P22","P01","P02","P12","Phi0","Phi1","Phi2"])
par=list([Nl,Na,Tmax,Tintro,k,parm,nlarve,GenoSurv,PhenoSurv,h,P00,P11,P22,P01,P02,P12,phi0,phi1,phi2])
string=chaine2(par,nom)


file2 = open(line2,"a")
file2.write(string)
file2.close()

# Fichier proba
line5 = path + "/proba_X0.txt"
line6 = path + "/proba_X1.txt"

if dd == "T":
    file5 = open(line5, "w")
    file5.write("P_X0")
    file5.close()

    file6 = open(line6, "w")
    file6.write("P_X1")
    file6.close()
    lineproba = [line5, line6]

    file7 = open(path+"/dist_proba.txt", "w")
    file7.write("X0;X1;parm\n")
    file7.close()

    file7 = open(path+"/dist_proba.txt","a")
    for i in list(range(0,k+1)):
        file7.write(str(proba_X0[i])+";"+str(proba_X1[i])+";"+{2}+"\n")
        file7.close()

###############################################################################
#Boucle principale

#Initialisation du compteur général
t = 1

#Tant que le temps n'a pas atteint le temps max
while t <= Tmax:
    #On réalise de cycle de vie des individu et on le stocke dans pop
    #Cylce de vie = mort + métamorphose
    
    pop = cycle(listLarve,listMale,listFemelle,GenoSurv,PhenoSurv, phenoeff, freq[0].n ,k, parm,dd,proba_X0, proba_X1)
    
    #On stocke chaques listes après survie dans la liste correspondante
    listLarve = pop[0]
    listMale = pop[1]
    listFemelle = pop[2]
    
    
    #Execution du cycle de reproduction produisant les nouvelles larves
    rep = repro(listFemelle,listMale,nlarve,h,MatH, matdom,cost)
    listLarve = rep[0]
    Vrej = rep[1]
    Vacc = rep[2]
    
    file3 = open(line3,"a")
    file3.write(chaine3(Vacc,t))
    file3.close()
    
    file4 = open(line4,"a")
    file4.write(chaine3(Vrej,t))
    file4.close()
    
    
    
    
    file5 = open(line5, "a")
    file5.write(pop[3])
    file5.close()
    
    file6 = open(line6, "a")
    file6.write(pop[4])
    file6.close()
    
    #On affiche le pas de temps
    print(t)
    #Si Tmut est égal à Tintro
    if t==Tintro:
        
        mut = 1
        while mut <= nmut:
            j = rand.randrange(0,len(listLarve))
            #Cette larve devient la larve mutante
            listLarve[j]=larvemut2
            mut += 1
    
    t += 1
    
    
    #Calcul des férquences alléliques , phénotypique et génotypiques des 2 stades
    freq = [count(listLarve, etat(0,0,0,0,0,0,0,0,0,0,0,0,len(listLarve),{0},{1}),stade="Larve"), count(listMale+listFemelle, etat (0,0,0,0,0,0,0,0,0,0,0,0,len(listMale+listFemelle),{0},{1}),stade="Adulte")]
    #freq = [count(listLarve, etat(0,0,0,0,0,0,0,0,0,0,0,0,len(listLarve),"test","test"),stade="Larve"), count(listMale+listFemelle, etat (0,0,0,0,0,0,0,0,0,0,0,0,len(listMale+listFemelle),"test","test"),stade="Adulte")]

    #Calcul du fardeau pour le temps suivant
    
    #Si df="T"
    if df == "T":
        #On calcule le fardeau à n+1 à partir des fréquences alléliques
        if fp == "F":
            dphi1 = phi(phi1,freq[1].fbb,(freq[1].fab+freq[1].fbc),r)
            dphi2 = phi(phi2, freq[1].fcc,(freq[1].fac+freq[1].fbc),r)
        elif fp == "T":
            dphi1 = phi(phi1,freq[1].fbb+freq[1].fbc,freq[1].fab,r)
            dphi2 = phi(phi2, freq[1].fcc,freq[1].fac,r)
        
        #Si le fardeau calculé est différent de 0 
        if dphi1!=0 :
            #On stocke le fardeau dans la matrice
            phi1 = dphi1
            GenoSurv[1,1] = 1-phi1
            
        if dphi2!=0:
            phi2 = dphi2
            GenoSurv[2,2] = 1-phi2
        
       
    #Affiche le fardeau
    #print(phi1)
    #print(phi2)
    
    #On ajoute au fichier de sortie les informations du tour
    stringL=chaine(freq[0],t,"Larve","NA","NA")
    stringA=chaine(freq[1],t,"Adulte",phi1,phi2)
    file = open(line,"a")
    file.write(stringL)
    file.write(stringA)
    file.close()
    
    #On incrémente les 2 compteurs
    
    
    
    


t1=time.time()

dt=t1-t0

file2 = open(line2,"a")
file2.write(str(dt))
file2.close()

skewplot(alpha,mu[0],sigma[0])
skewplot(alpha,mu[1],sigma[1])

print(dt)
###############################################################################
