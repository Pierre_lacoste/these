# -*- coding: utf-8 -*-
"""
Created on Mon Aug  3 11:40:56 2020

@author: lacos
"""
import numpy as np

"""Paramètres"""

"Nombre de larves"
Nl = 8000
"Nombre d'adulte /2"
Na = 1000

nmut = 4000

Tmax= 300
Tintro=1000

k=10000

parm=[4,4]

nlarve = 5

#Matrices de survie
df = "F"
phi0 = 0.05

#phi1 = {0}
phi1 = 0.9

fp = "F"

phi2 = 0.8
r = 5
#GenoSurv = np.array([[0.95,0.95,0.95],[0.95,0.10,0.95],[0.95,0.95,0.20]])
#sil= {1}
sil= 0.9
bic= 0.9
tar= 0.9
PhenoSurv = [sil,bic,tar]
#densité dependance

dd = "T"

alpha = 0
mu = [1000,1200]
sigma = [200,200]


#Matrice de dominance
matdom = np.array([[0,1,2],[1,1,1],[2,1,2]])

#Heterogamie

h = "T"
P00 = 0.1
P11 = 0.1
P22 = 1
P01 = 1
P02 = 1
P12 = 1

cost = "F"
#Reco = "Pheno"
###############################################################################

