# -*- coding: utf-8 -*-
"""
Created on Wed Jul 15 10:01:04 2020

@author: lacos
"""

#Classe compteur 

class etat:
    """Compteur qui reunit toutes les infomations de notre système"""
    
    def __init__(self,fa,fb,fc,faa,fab,fac,fbc,fbb,fcc,fA,fB,fC,n,replicat,parm):
        #freq alleliques
        self.fa = fa
        self.fb = fb
        self.fc = fc
        #freq genotypique
        self.faa = faa
        self.fab = fab
        self.fac = fac
        self.fbc = fbc
        self.fbb = fbb
        self.fcc = fcc
        #freq phenotypique
        self.fA = fA
        self.fB = fB
        self.fC = fC
        #Taille de la pop
        self.n = n
        #replicat
        self.replicat = replicat
        self.parm = parm
        
    def __str__(self):
        return "Frequences alléliques:\nFa = {}\nFb = {}\nFc = {}\nFrequences génotypiques\nFaa = {}\nFab = {}\nFac = {}\nFbc = {}\nFbb = {}\nFcc = {}\nFréquences phenotypique\nFA = {}\nFB = {}\nFC = {}\nTaille de la populaion : {}\nReplicat = {}\n Parametre = {}".format(
            self.fa,self.fb,self.fc,self.faa,self.fab,self.fac,self.fbc,self.fbb,self.fcc,self.fA,self.fB,self.fC,self.n,self.replicat,self.parm)

