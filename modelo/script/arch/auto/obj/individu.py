# -*- coding: utf-8 -*-
"""
Created on Wed Jun 24 15:39:24 2020

@author: lacos
"""
import numpy as np

#Défnition des classes

class Larve:
    """Classe  de mes individus"""
    
    def __init__(self, x1, x2, X):
        """ Definition des attributs de la classe individu"""
        
        self.age = 0
        self._x1 = x1
        self._x2 = x2
        self._pheno = X
    
    #On deffinie les méthodes pour encapsuler génotype , phénotype et age
    def _get_x1(self):
        """Methode d'appel de l'attribut x1"""
        return(self._x1)
    
    def _get_x2(self):
        """Methode d'appel de l'attribut x2"""
        return(self._x2)
    
    def _get_geno(self):
        """Methode d'appel de l'attribut génotype"""
        print("[",self._x1,",",self._x2,"]")
    
    def _get_pheno(self):
        """Methode d'appel de l'attribut phénotype"""
        
        return (self._pheno)
    
    #Ces attribut ne doivent pas être modifié donc on ne defini pas de methode set 
    
    #On lie les attribut à une propriété qui pointe vers nos méthodes de lecture des attributs
    x1 = property(_get_x1)
    x2 = property(_get_x2)
    geno = property(_get_geno)
    pheno = property(_get_pheno)
    
###############################################################################    

class Male(Larve):
    """ La classe Male definie des invidus males adultes
        c'est une sous-classe de la classe Larve"""
    
    def __init__(self, x1, x2, X, age):
        """ Definition des attributs de la classe Male"""
        
        #heritage des attributs de la classe larve 
        Larve.__init__(self, x1, x2, X)
        
        #Attribut propre à la classe 
        self.age = age
        self._sexe ="M"
    
    #Encapsulation du sexe
    def _get_sexe(self):
        print (self._sexe)    
    sexe = property(_get_sexe)
    
    #Méthode pour compter le nombre d'acouplements d'un individu
    def accouplement(self):
        """Compteur du nombre d'accouplement"""
        self.nacc += 1
    
    def viellir(self):
        """Methode de vielleissement"""
        self.age += 1


###############################################################################

class Femelle(Larve):
    """ La classe Femelle definie des invidus femelles adultes
        c'est une sous-classe de la classe Larve"""
    
    def __init__(self, x1, x2, X, age):
        """ Definition des attributs de la classe Femelle"""
        
        #heritage des attributs de la classe larve 
        Larve.__init__(self, x1, x2, X)
        
        #Attribut propre à la classe 
        self.age = age
        self._sexe = "F"
        self.nacc = 0
        self.malegeno = "NA"
        self.offspring = 0
    
    #Encapsulation du sexe
    def _get_sexe(self):
        print (self._sexe)   
    sexe = property(_get_sexe)
    
    def viellir(self):
        """Methode de vielleissement"""
        self.age += 1
    
    




        

    
  