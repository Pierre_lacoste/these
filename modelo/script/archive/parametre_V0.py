# -*- coding: utf-8 -*-
"""
Created on Mon Aug  3 11:40:56 2020

@author: lacos
"""
import numpy as np

"""Paramètres"""

Nl = 500
Na = 500

nmut = 500

Tmax=150

Tintro=1000

k=1000

parm=[4,4]

nlarve = 9

#Matrices de survie
df = "F"
phi0 = 0.05

phi1 = {0}


fp = "F"

phi2 = 0.8
r = 5
#GenoSurv = np.array([[0.95,0.95,0.95],[0.95,0.10,0.95],[0.95,0.95,0.20]])
sil= 0.9
bic= 0.9
tar= 0.9
PhenoSurv = [sil,bic,tar]

#Matrice de dominance
matdom = np.array([[0,1,2],[1,1,1],[2,1,2]])

#Heterogamie


h = "T"

P00 = 0.90
P11 = 0.45
P22 = 0.1
P01 = 0.90
P02 = 0.90
P12 = 0.90

cost = "T"

#Reco = "Pheno"
###############################################################################

