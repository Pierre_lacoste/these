---
title: "Analyse mate choice"
author: "Pierre Lacoste"
date: "26/09/2022"
output: pdf_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

```{r,include=FALSE}
rm(list=ls())
```


## Initialisation

Exemple: 

```{r}
source("Fonction_mate_choice.R")
f = c("aquinia","aquinia","derasa","derasa")
m = c("aquinia", "derasa","aquinia","derasa")
Mate = c(9,0,1,10)
Total = c(12,6,6,12)

data = data.frame(f,m,Mate,Total)

l=length(data$f)
```
On construit un tableau avec en colone:

- 1 les femelles 
- 2 les males  
- 3 les effectifs pour chaques accouplements ($m_{ij}$)
- 4 Le totals de test pour chaques accouplement ($N_{ij}$)

## Modèles et valeurs théoriques 

### Tous différents:

La probabilité de chaques accouplement est différentes des autres.

Calcul de la Proba théorique : $P_{ij}=m_{ij}/N_{ij}$

```{r}
M1 = rep(0,l)
for (i in 1:l){
  M1[i]= data[i,3]/data[i,4]
}
M1 = c(M1,4)
```

### Tous égaux

La probabilité de chaque accouplement est la même (Random mating)

Calcul de la Proba théorique : $P_{ij}=m/N$

```{r}
M2 = rep(sum(data$Mate)/sum(data$Total),l)
M2 = c(M2,1)
```


### Mate choice symétrique 

Pour ce modèle on teste s'il y a un mate choice (Homogame ou hétérogame)

Calcul de la Proba théorique : pour les couples homogames $P_{i=j}=\sum m_{i=j}/\sum N_{i=j}$ et pour les couples hétérogames $P_{i\neq j}=\sum m_{i\neq j}/\sum N_{i\neq j}$

#### Implémentation 

On commence par identifier les couples homogames et hétérogames en créant un bouléen
```{r}
boul =  data[,1]==data[,2]
```
Le vecteur contient `TRUE` quand le couple est homogame et `FALSE` quand le couple est hétérogame.

On ajoute ce vecteur au tableau pour pouvoir l'utiliser 

```{r}
data[,5]=boul
```

On calcule les probas
```{r}
Phomo = sum(subset(data,boul==T)[,3])/sum(subset(data,boul==T)[,4])
Phetero = sum(subset(data,boul==F)[,3])/sum(subset(data,boul==F)[,4])

M3 = rep(0,l)
for (i in 1:l){
  if (data[i,5]==T){
     M3[i]=Phomo
  }
  else {
    M3[i]=Phetero
  }
}

M3 = c(M3,2)
```

## Log vraissemblance 

### Calcul
Pour un modèle donnée la log vraissemblance est :
$\sum (m_{ij}*\log(\hat m_{ij})+(N_{ij}-m{ij})*\log(1-\hat m_{ij}))$

Pour le modèle 1

```{r}
ll_M1=loglike(data,M1,l)
ll_M2=loglike(data,M2,l)
ll_M3=loglike(data,M3,l)
```

### AIC

On calcule l'AIC pour avoir une idée de quel modèle est le meilleur:

```{r}
M1_AIC = AIC(ll_M1, M1)
M1_AICc = AICc(ll_M1, M1)
```

## Comparaison des modèles

L'objectif est de calculer une p-value pour la comparaison de chaque modèle:

Calcul de la statistique: $G_{m_{i}m_{j}} = 2*(loglike(M1)-loglike(M2))$

```{r}
G_M1_M2 = G_stat(ll_M1,ll_M2)
```

A partir de la statistique on peut claculer une p-value en utilisant la fonction : `pchisq`

```{r}
pchisq(G_M1_M2, df=M1[length(M1)]-M2[length(M2)], lower.tail=FALSE)
```



