---
title: "Chapitre 1 Outline"
author: "Pierre Lacoste"
date: "04/10/2022"
output: word_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

## Color Polymorphism

Understand maintenance of polymorphism

Model =\> color polymorphism

Color play a role in interaction =\> cue in mate choice and predation

mate choice can favor polymorphism =\> disassortative mating =\> NFDS

Model : *Heliconius numata*

previous studdy:

Polymorphism result from the equilibrium between

-\> Aposematic coloration and Mimetic advantage

-\> genetic load due to inversion

-\> Disassortative mating

Question: Is this model valid all accros the rage ? Can we explain coexistance and frequency of different form in all the range only with disassortative mating, mimetic interaction and genetic load ?

Studdy french guyana popultion and :

## Objectif

Identify disassortive mating in the population of french guyana and test weather this mate choice allow to maintain polymorphism in the population.
