---
title: "Predation"
author: "Pierre Lacoste"
date: "20/01/2022"
output: pdf_document
---

```{r}
rm(list = ls())
library(ggplot2)
```

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

# Chargement des données et inspection

On commenec par charger les données.

```{r}
data = read.csv("./predation_v2.csv", sep = ";", dec = ",", header = T)
data_site = read.csv("./sites.csv", sep = ";", dec = ",", header = T)
```

On inspecte le tableau.

```{r}
str(data)
```

Les données on bien été chargées, maintenant faut les inspecter.

```{r}
for (i in c(2,4:6)) {
  data[,i] = as.factor(data[,i])
}
str(data)
```

On veux verifier les effectifs

Effectifs des prédations à partir des photo.

```{r}
summary(data$zone)
sum(summary(data$zone))
```

Effectif à partir des notes de terrain

```{r}
eff = NULL
site = NULL
for (j in levels(as.factor(data_site$zone))) {
  site = c(site,j)
  eff = c(eff,sum(subset(data_site, zone == j)$total_pred))
}
total = data.frame(site,eff)
total
```

On à 4 prédations de plus au total à partir des photos, plusieurs possibilités:

-   plusieurs photos de la même prédation
-   Erreur dans les notes de terrain
-   Prise de photo de marque litigieuses

# Nettoyage du jeu de données

On va trier par la nature des prédations, on retrire les prédation fake car elles sont due à l'evironement (branches ou feuilles) et non à une attaque par un prédateur.

```{r}
data_c = subset(data, type_attaque != "fake")
summary(data_c$zone)
```

## Recupération des données manquantes

Problème ! Certaines attaque n'ont pas pu être assignées à un morphe car les ailles avaient été arrachées et n'ont pas été retrouvées, il n'est donc pas possible de connaitre le morphe à partir des photos... Néanmoins sur le terrain on a identifié ces prédations grâce à la logique de pose des leurres. L'information du morphe des non indentifés est donc stocké dans le tableau data_site. Dans ce tableau on peut récupérer le nombre de leurre de chaque morphe qui ont été attaqués par jour et donc trouver les morphes qui manquent dans notre jeux de données produit à partir des photos.

En comparant les données dans les deux tableau j'ai pu compléter les données manquantes dans le tableau issu des photos.

Je verifie que pour chaque jour de chaque zone les deux tableaux sont identiques.

```{r}
for (i in levels(as.factor(data$date))) {
  s=summary(subset(data_c,date == as.character(i))$Morph)
  print(i)
  print("Photos")
  print(s)
  print("Notes")
  print(c(sum(subset(data_site,date == i)$num_pred),sum(subset(data_site,date == i)$sil_pred)))
  print("=================================================================")
}

```

Après remplissage des données manquantes on est bon. Il manque juste une donnée à Kaw qu'il faut que je verifie sur mon carnet de terrain (il manque une photo, surement du à un fil seul donc il faut que je sorte cet donnée du tableau car on ne peut pas identifier le type de prédation)

## Calcul du taux d'attaque mené par des oiseaux:

```{r}
summary(data_c$type_attaque)
```

Il reste 234 évènements de prédation dont 28 due à des inectes et 205 à des oiseaux.

Calcul du taux d'attaque par les oiseaux:

```{r}
t_oiseau = summary(data_c$type_attaque)[4]/data_site$total_col[13]
t_oiseau
```

Le taux d'attaque par les oiseaux est donc de 11,5% ce qui est plus de 2 fois suppérieurs aux 5% attendu!

Je peux calculer ce taux d'attaque par les oiseaux pour chaque site:

```{r}
token = 1
for (i in levels(data_c$zone)){
  
  d = subset(data_c, zone == i) #Sous tableau par zone
  
  eff_o = as.numeric(summary(d$type_attaque)[4]) #Nombre de prédation d'oiseaux
  eff_tot = sum(subset(data_site,zone == i)$total_col) #Nombre total de leurres
  
  #Taux d'attaque d'oiseaux
  total$t_o[token] = round(as.numeric(eff_o)/eff_tot,3)
  token = token+1
}

#Taux d'attaque total

total$t_o[4]=round(as.numeric(summary(data_c$type_attaque)[4])/data_site$total_col[13],3)

total
  
```

## Comparaison des 2 morphes:

Maintenant qu'on a les taux de prédation de chaque site, je vais regarder le taux d'attaque de chaque morphe parmis les attaques observées.

Je commence par enlever les prédation causé par des insectes pour ne garder que celle par les oiseaux

```{r}
data_oiseau = subset(data_c, type_attaque == "oiseau")

token2 = 1
for (i in levels(data_oiseau$zone)) {
  d = subset(data_oiseau, zone == i)
  
  sil = sum(d$Morph == "sil")
  num = sum(d$Morph == "num")
  
  total$t_sil[token2] = sil
  total$t_num[token2] = num
  token2=token2+1
}

total$t_sil[4] = sum(data_oiseau$Morph == "sil")
total$t_num[4] = sum(data_oiseau$Morph == "num")

total
```

On vas sauvegarder le tableau total dans un fichier csv

```{r}
write.csv2(total,file = "frequences.csv")
```

# Regression logistique

Il nous faut savoir si les différences entre les morphes sont significatives pour chaque sites. Pour cela on vas faire une regression logistique pour comparer les deux morphes.

## Kaw

On commence par le site de Kaw.

```{r}
kaw = subset(data_oiseau, zone == "Kaw")
par(cex.axis = 1.5)
barplot(summary(kaw$Morph), col = c("black","white"), ylim = c(0,50))
#dev.print(device = png, file = "Kaw.png", width = 600)
```

On complète les données avec les échec que l'on à pas consignés. Pour cela on va recupérer les effectifs de leurres collectés dans le tableau `data_site.csv`

```{r, warning=FALSE}
kaw_echec = kaw[200,]

kaw_echec$type_attaque = as.character(kaw_echec$type_attaque)
kaw$type_attaque = as.character(kaw$type_attaque)
kaw_echec$zone = as.character(kaw_echec$zone)
kaw$zone = as.character(kaw$zone)
kaw_echec$Morph = as.character(kaw_echec$Morph)
kaw$Morph = as.character(kaw$Morph)



intact_eff_sil = sum(data_site$sil_col[9:12])-sum(data_site$sil_pred[9:12])
intact_eff_num = sum(data_site$num_col[9:12])-sum(data_site$num_pred[9:12])

for (i in 1:intact_eff_sil) {
  kaw_echec[i,] = c(NA,"Kaw",NA,"echec",NA,"sil",NA)
}

for (i in (intact_eff_sil+1):(intact_eff_sil+intact_eff_num)) {
  kaw_echec[i,] = c(NA,"Kaw",NA,"echec",NA,"num",NA)
}

kaw = rbind(kaw,kaw_echec)


kaw$succes = kaw$type_attaque == "oiseau"
```

Ok maintenant On peut faire l'analyse.

```{r}
glm_kaw = glm(kaw$succes~as.factor(kaw$Morph), family = binomial("logit"))

anova(glm_kaw, test = "Chisq")
```

Ok on a une p-value à `Pr(>Chi)0.66` pour l'effet du morphe, il n'y a donc pas de différence significative pour entre les prédations subit par les morphes sur le site de kaw.

On fait la même chose pour les 3 autres sites.

## Maripa

```{r}
maripa = subset(data_oiseau, zone == "Maripa")
par(cex.axis = 1.5)
barplot(summary(maripa$Morph), col = c("black","white"), ylim = c(0,50))
#dev.print(device = png, file = "Maripa.png", width = 600)

maripa_echec = maripa[1000,]

maripa_echec$type_attaque = as.character(maripa_echec$type_attaque)
maripa$type_attaque = as.character(maripa$type_attaque)
maripa_echec$zone = as.character(maripa_echec$zone)
maripa$zone = as.character(maripa$zone)
maripa_echec$Morph = as.character(maripa_echec$Morph)
maripa$Morph = as.character(maripa$Morph)



intact_eff_sil = sum(data_site$sil_col[1:5])-sum(data_site$sil_pred[1:5])
intact_eff_num = sum(data_site$num_col[1:5])-sum(data_site$num_pred[1:5])

for (i in 1:intact_eff_sil) {
  maripa_echec[i,] = c(NA,"Maripa",NA,"echec",NA,"sil",NA)
}

for (i in (intact_eff_sil+1):(intact_eff_sil+intact_eff_num)) {
  maripa_echec[i,] = c(NA,"Maripa",NA,"echec",NA,"num",NA)
}

maripa = rbind(maripa,maripa_echec)


maripa$succes = maripa$type_attaque == "oiseau"
```

Ok maintenant On peut faire l'analyse.

```{r}
glm_maripa = glm(maripa$succes~as.factor(maripa$Morph), family = binomial("logit"))

anova(glm_maripa, test = "Chisq")
```

La p-value du test est de `Pr(>Chi)=0.52`, il n'y donc pas d'effet morphe significatif.

## St-Elie

```{r}
SE = subset(data_oiseau, zone == "St-Elie")
par(cex.axis = 1.5)
barplot(summary(SE$Morph), col = c("black","white"), ylim = c(0,50))
#dev.print(device = png, file = "SE.png", width = 600)

SE_echec = SE[1000,]

SE_echec$type_attaque = as.character(SE_echec$type_attaque)
SE$type_attaque = as.character(SE$type_attaque)
SE_echec$zone = as.character(SE_echec$zone)
SE$zone = as.character(SE$zone)
SE_echec$Morph = as.character(SE_echec$Morph)
SE$Morph = as.character(SE$Morph)



intact_eff_sil = sum(data_site$sil_col[6:8])-sum(data_site$sil_pred[6:8])
intact_eff_num = sum(data_site$num_col[6:8])-sum(data_site$num_pred[6:8])

for (i in 1:intact_eff_sil) {
  SE_echec[i,] = c(NA,"St-Elie",NA,"echec",NA,"sil",NA)
}

for (i in (intact_eff_sil+1):(intact_eff_sil+intact_eff_num)) {
  SE_echec[i,] = c(NA,"St-Elie",NA,"echec",NA,"num",NA)
}

SE = rbind(SE,SE_echec)


SE$succes = SE$type_attaque == "oiseau"
```

Ok maintenant On peut faire l'analyse.

```{r}
glm_SE = glm(SE$succes~as.factor(SE$Morph), family = binomial("logit"))

anova(glm_SE, test = "Chisq")
```

### Modèle complet:

Je vais essayer de retrouver le même resultat avec un modèle qui tient compte en premier temps le site et dans un 2e temps le morphe et l'interaction des deux.

Je commence par réunir tout les tableau complet (avec les succès et les echecs)

```{r}
data_complet = rbind(kaw,SE,maripa)
```

Maintenant on peut faire le modèle dont la formule sera `succes~zone*Morph` ce qui signifie on quantifie l'interaction zone morphe puis l'effet zonne et enfin l'effet additif morphe sachant les autres variables.

```{r}
glm_complet = glm(succes~as.factor(zone)*as.factor(Morph), data = data_complet, family = binomial("logit"))

anova(glm_complet, test = "Chisq")

```

On retrouve bien les resultats de chaques sites distant, il n'y a pas d'interaction entre le site et les morphes n'y d'effet additif du morphe sachant le site.

Graphique combiné

```{r}
site = c(total$site,total$site)
pred = c(total$t_sil,total$t_num)
morph = c(rep("silvana",4),rep("numata",4))

table_graph = data.frame(site,pred,morph)
g = ggplot(table_graph, aes(x=site, y=pred, fill = morph))+
  geom_bar(stat="identity",position=position_dodge())+
  scale_fill_manual(values=c('black','grey'),name = "Form")+
  labs(y="Number of predation",x="Site", colour = "Form")

#png("./Predation.png",height = 10,width = 20, units = "cm", res = 300)
g
```

### Predation en fonction des fréquence

On va recuperer les fréquences meusurées sur le terain et plot les pred obs en fonction des fréquences.

```{r}
freq = read.csv2("Frequence_morphe.csv")

tot = freq$Numata+freq$Melinaea_mediatrix+freq$Melinaea_mneme+freq$Silvana+freq$Melinaea_ludovica
tot = c(tot,tot)



eff_obs = c(freq$Numata+freq$Melinaea_mediatrix+freq$Melinaea_mneme,freq$Silvana+freq$Melinaea_ludovica)
form = c(rep("Numata",3),rep("Silvana",3))
pred2 = c(total$t_sil[1:3],total$t_num[1:3])
freq_pred = c(32/61,36/66,35/78,29/61,30/66,43/78)

freq_obs = eff_obs/tot

m0 = lm(freq_pred~freq_obs)
anova(m0)
summary(m0)
shapiro.test(residuals(m0))
bptest(m0)
dwtest(m0)

jpeg("./Predation_rapport.jpg",height = 15,width = 20, units = "cm", res = 300)

plot(freq_pred~freq_obs, ylim =c(0,0.6), col = c("black","black","black","grey","grey","grey"), pch = 19, ylab = "Predation rate", xlab = "Frequency")
abline(a= 0.45704, b=0.08592, col = "black")

legend(x= 0.6,y=0.2, legend = c("silvana group","numata group"), col = c("grey","black"), pch = 19)

```

## Chi²

On va utiliser le tableau produit precedement pour faire un test du Chi² de conformité, on cherche à verifier si les taux de prédation observé different de taux qu'on aurait s'il n'y avait pas de choix.

### Kaw

```{r}
chisq.test(total[1,4:5],p = c(0.5,0.5))
```

### Maripa

```{r}
chisq.test(total[2,4:5],p = c(0.5,0.5))
```

### St-Elie

```{r}
chisq.test(total[3,4:5],p = c(0.5,0.5))
```

### Total

```{r}
chisq.test(total[4,4:5],p = c(0.5,0.5))
```
